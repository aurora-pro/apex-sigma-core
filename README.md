[![apex](https://i.imgur.com/TRSdGni.png)](https://auroraproject.xyz/)

<p align="center">
  <a href="https://discordapp.com/invite/Ze9EfTd">
    <img src="https://discordapp.com/api/guilds/200751504175398912/widget.png?style=shield" />
  </a>
</p>

# Sigma Core

The heart of the Sigma Discord Bot.

## Plugins

If you are looking for plugins (or you want to write one), check out the [plugins repo](https://github.com/aurora-pro/apex-sigma-plugins).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).